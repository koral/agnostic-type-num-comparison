/* Function for encoding the floating point numbers  */
/* (because decoding is symmetric this func will serve also this purpose) */

int32_t encode_float(float f) {
	int32_t *p;

	p = (int32_t *) &f;

	if (f < 0)
		*p = 0x80000000 | ~*p;

	return *p;
}
